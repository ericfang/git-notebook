const path = require('path');

function resolve(dir) {
    return path.join(__dirname, dir);
}

module.exports = {
    publicPath: './',
    lintOnSave: false,
    devServer: {
        // can be overwritten by process.env.HOST
        host: '0.0.0.0',
        port: 20001,
    },
    chainWebpack: config => {
        config.resolve.alias
            .set('@', resolve('src'))
            .set('src', resolve('src'))
            .set('common', resolve('src/common'))
            .set('components', resolve('src/components'));
    },
    pluginOptions: {
        electronBuilder: {
            builderOptions: {
                mac: {
                    icon: './public/app.png'
                },
                productName: 'GitNotebook',
                win: {
                    "icon": "./public/app.ico",
                    "requestedExecutionLevel": "highestAvailable",
                    "target": [
                        "nsis",
                        "zip"
                    ]
                },
                nsis: {
                    "oneClick": false,
                    "allowToChangeInstallationDirectory": true,
                    "perMachine": true
                }
            }
        }
    },
    css: {
        loaderOptions: {
            less: {
                javascriptEnabled: true,
            }
        }
    }
};