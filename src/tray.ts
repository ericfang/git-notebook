import * as path from "path";
import {Menu, Tray} from "electron";

export default function setTray(app, mainWindow) {

    const trayMenuTemplate = [
        {
            label: '退出',
            click: () => {
                app.quit();
            }
        }
    ];

    const iconPath = path.join(__dirname, './bundled/app.ico');
    const appTray = new Tray(iconPath);

    const contextMenu = Menu.buildFromTemplate(trayMenuTemplate);

    mainWindow.show();

    appTray.setToolTip("git-notebook");

    appTray.setContextMenu(contextMenu);

    appTray.on("click", () => {
        mainWindow.show();
    })

    return appTray;
}