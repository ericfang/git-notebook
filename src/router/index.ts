import {
    createRouter,
    createWebHistory,//history模式
    createWebHashHistory, Router//hash模式
} from 'vue-router'

const router: Router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {path: '/', redirect: "/home"},
        {path: '/home', component: () => import("@/pages/home.vue")},
    ],
});

router.beforeEach((t, f, n) => {
    n();
});

export const ROUTER = router;
export default router;
