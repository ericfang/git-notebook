import {CONFIG_FILE_PATH} from "@/electron/const";
import {deleteFile, saveFile} from "@/utils/main/file.uitls";
import {MAIN_STORE} from "@/electron/main";
import {computed} from "vue";

const fs = require("fs");
const path = require("path");

/**
 * 读取配置文件
 */
export const readApplicationConfig = function (): ApplicationConfig {
    const configPath = path.resolve(CONFIG_FILE_PATH);
    if (!fs.existsSync(configPath)) {
        saveFile(configPath, JSON.stringify(new ApplicationConfig()));
    }
    const json = fs.readFileSync(configPath);
    try {
        return JSON.parse(json);
    } catch (e) {
        console.warn("JSON parse fault");
    }
    return null;
}

/**
 * 写配置文件
 * @param json
 */
export const writeApplicationConfig = function (json: ApplicationConfig) {
    const configPath = path.resolve(CONFIG_FILE_PATH);
    let config = readApplicationConfig();
    Object.assign(config, json);
    saveFile(configPath, JSON.stringify(config));
    MAIN_STORE.config = config;
}

/**
 * 获取选中笔记节点
 */
export const getSelectedNote = function () {
    const {config, notes} = MAIN_STORE;
    return notes.find(item => item.name === config.selectedNoteName);
}

/**
 * 应用配置
 */
export class ApplicationConfig {
    windowWidth?: number = 1280;
    windowHeight?: number = 800;
    selectedNoteName?: string = "";
    theme: string = "";
}



