import {ApplicationConfig, readApplicationConfig} from "@/electron/application";
import "./application";
import "./ipc-handler";
import {mkDir} from "@/utils/main/file.uitls";
import {NOTES_DIR} from "@/electron/const";
import {MainStore} from "@/utils/main/main.declare";

const fs = require("fs");
const path = require("path");

// 全局参数
export const MAIN_STORE: MainStore = {
    config: new ApplicationConfig(),
    win: null,
    notes: [],
}

// 初始化配置
MAIN_STORE.config = readApplicationConfig();

// 检查笔记文件夹是否存在，不存在则新建文件夹
if (!fs.existsSync(path.resolve(NOTES_DIR))) {
    mkDir(path.resolve(NOTES_DIR));
}









