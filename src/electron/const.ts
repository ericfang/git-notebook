import {app} from "electron"
import path from "path"

export const APP_PATH = path.join(app.getPath('exe'), "../")
export const WIN_CONFIG_DIR = path.join(APP_PATH, "./application");
export const MAC_CONFIG_DIR = path.join(APP_PATH, "./application");
export const CONFIG_FILE_NAME = "application.json";
export const CONFIG_FILE_PATH = path.join(WIN_CONFIG_DIR, CONFIG_FILE_NAME);
export const IS_DEVELOPMENT = process.env.NODE_ENV !== 'production';
export const NOTES_DIR = path.join(APP_PATH, "./notes");
export const IMAGE_CONTAINER = '.image-container';
export const EXCLUDE_DIR = ['.git', IMAGE_CONTAINER];


console.log("APP_EXE:", app.getPath('exe'));
console.log( "APP_PATH:" , APP_PATH );
console.log( "NOTES_DIR:" , NOTES_DIR );