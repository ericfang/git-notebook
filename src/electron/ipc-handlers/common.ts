import {ipcMain} from "electron";
import {MAIN_STORE} from "@/electron/main";
import {ApplicationConfig, writeApplicationConfig} from "@/electron/application";

// 获取应用全局配置信息
ipcMain.handle("getApplicationHandler", (e) => {
    return MAIN_STORE.config;
});

// 设置全局配置
ipcMain.handle("setApplicationHandler", (e, config: ApplicationConfig) => {
    MAIN_STORE.config = config;
    writeApplicationConfig(config);
});

export default {};