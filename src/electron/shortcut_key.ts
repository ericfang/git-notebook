import {BrowserWindow, globalShortcut} from "electron";


const isDevelopment = process.env.NODE_ENV !== 'production'

export const registerShortCut = function (win: BrowserWindow) {
    if (isDevelopment) registerDevelopShortCut(win);
    if (!isDevelopment) registerProductShortCut(win);
    registerDevProdShortCut(win);
}

/**
 * 添加开发模式下的快捷键
 * @param win
 */
const registerDevelopShortCut = function (win: BrowserWindow) {
    globalShortcut.register("CommandOrControl+shift+f12", function () {
        win.webContents.openDevTools();
    })
}

/**
 * 添加生产模式下的快捷键
 * @param win
 */
const registerProductShortCut = function (win: BrowserWindow) {

}

/**
 * 添加所有模式下的快捷键
 * @param win
 */
const registerDevProdShortCut = function (win: BrowserWindow) {

}

