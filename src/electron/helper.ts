import {MAIN_STORE} from "@/electron/main";
import {NoticeMessageType} from "@/utils/common.declare.utils";

// 通知前端刷新笔记列表
export const noticeRefreshNotesToRenderer = () => {
    if (!MAIN_STORE.win) return;
    MAIN_STORE.win.webContents.send("onRefreshNotes");
};

// 通知前端刷新笔记目录树
export const noticeRefreshDirectoriesToRender = () => {
    if (!MAIN_STORE.win) return;
    MAIN_STORE.win.webContents.send("onRefreshNoteDirectories");
}

/**
 * 通知前端弹出 message 提示
 * @param message   消息
 * @param type  提示类型
 * @param duration 显示延迟
 */
export const noticeMessageToRenderer = (
    message: string,
    type: boolean | NoticeMessageType = "success",
    duration: number = 3000
) => {
    if (!MAIN_STORE) return;
    let tp = type;
    if (type === true) tp = "success";
    if (type === false) tp = "error";
    MAIN_STORE.win.webContents.send("onNoticeMessage", message, tp, duration);
};

/**
 * 通知触发 store
 * @param event
 * @param action
 * @param args
 */
export const noticeTriggerStore = (event: string, action = 'dispatch', ...args) => {
    if (!MAIN_STORE.win) return;
    MAIN_STORE.win.webContents.send("onTriggerStore", event, action, ...args);
}