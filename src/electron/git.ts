import {getSelectedNote} from "@/electron/application";
import {commitAndPush} from "@/utils/main/git.utils";
import {debounceRun} from "@/utils/throttle-debounce.utils";
import {DirTree} from "@/utils/main/file.uitls";

/**
 * 同步笔记
 */
export const syncNote = (note?: DirTree) => {
    if (!note) note = getSelectedNote();
    if (!note) return;
    debounceRun("SyncNote_Main_" + note.name, async () => {
        const res: boolean = await commitAndPush(note.path);
    }, 2000);
}