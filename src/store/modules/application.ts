import {IpcRenderer} from "@/utils/render/common";
import {toRaw} from "vue";

const state = {
    config: null
}

const mutations = {

    // 同步 config 文件
    async getConfig(state) {
        state.config = await IpcRenderer.invoke("getApplicationHandler");
    },

    // 设置配置
    setConfig(state, {config, syncToFile = true}) {
        state.config = Object.assign({}, state.config, config);
        if (syncToFile) IpcRenderer.invoke("setApplicationHandler", toRaw(state.config));
    },

}

const actions = {}

const getters = {}

export default {
    state, mutations, actions, getters
}