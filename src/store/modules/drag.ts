const state = {
    target: null,
    source: null,
}

const mutations = {
}

const actions = {
    // 设置拖拽目标
    setTarget({state}, target) {
        state.target = target;
    },
    // 设置拖拽目标源
    setSource({state}, source) {
        state.source = source;
    },
    // 情况拖拽
    clearTargetSource({state}) {
        state.target = null;
        state.source = null;
    }
}

const getters = {}

export default {
    state, mutations, actions, getters
}