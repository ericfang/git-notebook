import {createStore} from "vuex";
import drag from "@/store/modules/drag";
import note from "@/store/modules/note";
import application from "@/store/modules/application";


const store = createStore({
    modules: {
        drag,
        note,
        application,
    }
});

export default store;
export const STORE = store;
