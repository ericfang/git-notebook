import {ApplicationConfig} from "@/electron/application";
import {BrowserWindow} from "electron";
import {DirTree} from "@/utils/main/file.uitls";

export interface MainStore {
    config: ApplicationConfig,
    win: BrowserWindow,
    notes: DirTree[],
}