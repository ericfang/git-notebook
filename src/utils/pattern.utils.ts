export class Pattern {

    constructor(pattern, message) {
        this.pattern = pattern;
        this.message = message;
    }

    pattern: RegExp;
    message: string;
}

/**
 * 验证正则
 * @param text
 * @param pattern
 */
export const validatePattern = (text: string, pattern: Pattern): boolean | string => {
    if (pattern.pattern.test(text)) {
        return true;
    }
    return pattern.message;
}


export const PATTERN_FILE_NAME = new Pattern(
    /^[^\\\?\*\|\"\"\<\>\:\/]{1,256}$/,
    '不能含有 ? \\ * | “ < > : / 字符'
);


