export const dragObject = {
    target : null,
    source : null,
}

export const setTarget = (e, item) => {
    dragObject.target = item;
}

export const setSource = (e, item) => {
    dragObject.source = item;
}

export const clearTargetAndSource = () => {
    dragObject.target = null;
    dragObject.source = null;
}