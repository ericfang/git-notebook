/**
 * filter 树状结构
 * @param list
 * @param filter
 * @param childKey
 */
export const filterInTree = function <T>(list: T[], filter: (item: T) => any, childKey: string = 'children'): T[] {
    const res = Array<T>();
    for (let i = 0; i < list.length; ++i) {
        const item = list[i];
        if (filter.call(this, item)) {
            const t = Object.assign({}, item)
            res.push(t);
            if (t[childKey]) {
                t[childKey] = filterInTree(t[childKey], filter, childKey);
            }
        }
    }
    return res;
}
/**
 * 从树状结构中寻找一个节点
 * @param list     树状结构数据
 * @param finder    查找器
 * @param childKey  子节点名称
 * @param isDfs     是否使用深度遍历（ true 深度遍历，false 广度遍历）
 */
export const findInTree = function <T>(list: T[], finder: (item: T) => any, childKey: string = "children", isDfs = true): T {
    const stack = [...list];
    while (stack.length) {
        const t = isDfs ? stack.pop() : stack.shift();
        if (finder(t)) return t;
        if (t[childKey] && t[childKey].length) {
            stack.push(...t[childKey]);
        }
    }
    return null;
}