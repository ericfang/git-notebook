import * as electron from "electron";

export const ELECTRON = window.require("electron");
export const IpcRenderer = ELECTRON.ipcRenderer;

/**
 * 执行命令行信息
 * @param cmd
 * @param arg
 */
export const rendererExec = (cmd, ...arg) => {
    return IpcRenderer.invoke("execHandler", cmd, ...arg);
}

