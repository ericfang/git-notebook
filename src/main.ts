import {createApp} from 'vue'
import store from "@/store"
import App from './App.vue'
import ElementPlus from 'element-plus'
import {Splitpanes, Pane} from 'splitpanes'
import "./assets/fonts/iconfont.css"
import 'splitpanes/dist/splitpanes.css'
import 'element-plus/lib/theme-chalk/index.css'
import "./assets/styles/index.less"
import {IpcRenderer} from "@/utils/render/common";




(async () => {
    const app = createApp(App);

    const config = await IpcRenderer.invoke("getApplicationHandler");
    await store.commit("setConfig", {config, syncToFile: false});

    app.directive("gnb-autofocus", (el, directive) => {
        const input = el.querySelector("input");
        if (!input) return;
        input.focus();
    })

    app.use(store)
        .use(ElementPlus, {
            size: 'mini',
        })
        .component('splitpanes', Splitpanes)
        .component('pane', Pane)
        .mount('#app');
})()


