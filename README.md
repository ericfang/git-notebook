# git-notebook

> 一款基于 `git` 的笔记软件，利用 `git` 仓库建立属于自己的同步笔记本。

## 笔记功能

* 使用 `git` 作为在线笔记存放的仓库，实现不同设备之间的笔记同步；
* 使用开发人员常用的 `markdown` 编辑器进行笔记编辑；
* 实现图片截图的直接复制黏贴（类似 QQ 截图直接黏贴到笔记）


## 开源地址：

gitee源码: [https://gitee.com/ericfang/git-notebook](https://gitee.com/ericfang/git-notebook)

安装包下载：[https://gitee.com/ericfang/git-notebook/releases](https://gitee.com/ericfang/git-notebook/releases)


## 软件界面与操作

软件本身分为三个水平栏位；

第一个栏位：显示笔记文件夹目录结构；

第二个栏位：显示当前选择文件夹的文件以及目录；

点三个栏位：编辑文件的 `markdown` 编辑 和 预览；

![主界面](http://db.fangjc1986.com/git-notebook/1.png)

* 添加和编辑笔记

点击下图按钮进行笔记的添加和删除；

点击下拉列表切换笔记；

![主界面](http://db.fangjc1986.com/git-notebook/2.png)
![主界面](http://db.fangjc1986.com/git-notebook/3.png)

* 添加目录文件

左侧窗口为笔记文件夹目录树结构，可 `右键` 添加或直接添加 `添加文件夹` 按钮添加一级目录；

可直接拖动 `第二栏` 的文件或文件夹来自由移动文件到其他目录；

![主界面](http://db.fangjc1986.com/git-notebook/4.png)

* 添加笔记文件

点击 `文件夹` 和 `文件` 按钮来添加对应文件。

![主界面](http://db.fangjc1986.com/git-notebook/5.png)

* Markdown 编辑

点击编辑按钮进入 `markdown` 编辑按钮。

![主界面](http://db.fangjc1986.com/git-notebook/6.png)

编辑文件并按 `ctrl + s` 保存文件内容（笔记将自动同步提交到  `git` );

点击 `预览` 按钮回到预览模式。

![主界面](http://db.fangjc1986.com/git-notebook/7.png)


## BUG 提交

软件如有任何问题可提交 `issue` 或直接邮件给我 [fangjc1986@qq.com](fangjc1986@qq.com) 。

感谢您的使用与支持。



